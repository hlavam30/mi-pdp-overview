# Úvod do paralelizace

## Pojmy

- SIMD — single instruction multiple data
- MIMD — multiple instruction multiple data
- UMA, NUMA, CC-NUMA — sdílená paměť, distribuovaná paměť a virtuálně sdílená paměť
- RAM — random access machine

## Paralelní RAM (PRAM)

- Množina $$p$$ procesorů $$P_1, P_2, ..., P_p$$, každý má lokální paměť
- Pole $$m$$ sdílených paměťových buňěk $$M_1, M_2, ..., M_m$$
- Každý procesor přistupuje do jakékoliv buňky v čase $$O(1)$$


- Vstup: $$n$$ položek v buňkách sdílené paměti
- Výstup: $$n'$$ položek v buňkách sdílené paměti


- Procesory synchronně provádějí instrukce READ, LOCAL, nebo WRITE


- Ošetření konfliktů:
    - EREW - exclusive read exclusive write, žádné dva procesory nesmějí READ či WRITE do stejné buňky
    - CREW - concurrent read exclusive write, EREW s povoleným čtením z jedné buňky
    - CRCW - concurrent read concurrent write, jsou povoleny i zápisy
        - Priority-CRCW - procesory mají pevné priority a zápis je povolen jen procesoru s nejvyšší prioritou
        - Arbitraty-CRCW - zápis je povolen náhodnému procesoru
        - Common-CRCW - povoleno všem, pokud hodnoty, které se procesory snaží zapsat, jsou stejné, jinak jde o ilegální operaci

## Asynchronní PRAM (APRAM)

- Procesory pracují asynchronně, nemají společný CLK
- Vyžaduje explicitní synchronizaci (bariérami)
- Doba přístupu do paměti není jednotková

## Instruction-Level Paralelism

Určuje míru kolik instrukcí lze souběžně spustit. Několik instrukcí lze vykonat souběžně, jsou-li datově nezávislé.

Příklad:

```cpp
e = a + b
f = c + d
m = e * f
```

První dva řádky jsou paralelizovatelné pomocí ILP, jelikož jsou datově nezávislé.

- Datové závislosti:
    - true data dependency - instrukce čte registr, do kterého předchozí musí zapsat
    - output dependency - obě instrukce zapisují do stejného registru
    - anti-dependency - jedna instrukce musí přečíst dříve, než ji ho jiná přepíše
- Kompilátory hledají tyto ILP v co největší míře
- VLIW velmi usnadňuje hledání těchto ILP

---

## Zbytek

> TODO