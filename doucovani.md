# Poznámky k doučování 19. - 22. 5.

- MPI Mocninná metoda (přednáška 11)
- Naučit se Foxe
- Naučit se počítat škálovatelnost pro všechno! (především Cannon a Fox)
- [Co nejvíce příkladů!](procviceni.md)

## Co je potřeba vysvětlit

- PPS na složitějších strukturách (SF WH mřížky), škálovatelnost
- Jak dokázat konstrukčně uzlovou symetrii.
- Jak dokázat konstručkně, že kartézský součit zachovává uzlovou symetrii a jak aplikovat na 2D toroid?
- Naučit se parametry MPI funkcí. (očividně jsou skutečně potřeba)