# Summary

* [Úvod k MI-PDP Souhrnu](README.md)

#### Témata

* [Úvod do paralelizace](01-uvod.md)
    * [Paralelní RAM (PRAM)](01-uvod.md#paralelní-ram)
    * [Asynchronní PRAM (APRAM)](01-uvod.md#asynchronní-pram)
    * [Instruction-Level Paralelism](01-uvod.md#instruction-level-paralelism)
    * [Metriky složitosti sekvenčních algoritmů](01-uvod.md)
    * [Metriky paralelních algoritmů](01-uvod.md)
    * [Amdahlův a Gustafsonův zákon](01-uvod.md)
    * [Izoefektivní funkce](01-uvod.md)
* [OpenMP](02-openmp.md)
    * [Vlastnosti proměnných](02-openmp.md)
    * [Direktiva for](02-openmp.md)
    * [Direktiva task](02-openmp.md)
    * [Synchronizační nástroje](02-openmp.md)
    * [Direktiva atomic](02-openmp.md)
* C3S (Paralelní prohledávání stavového prostoru)
    * Statické rozdělení stavového prostoru
    * Dynamické vyvažování zátěže
    * Dynamický Master-Slave
* Paralelní řazení
* MPI
* Paralelní programování v OpenMP
* Propojovací sítě paralelních počítačů
* Kolektivní komunikační operace
* Paralelní redukce a sken
* Paralelní algoritmy v OpenMP a MPI


### Doplňky

* [Otázky k procvičení 📕](procviceni.md)
    * [Obecné a teorie](procviceni.md#obecné-z-úvodu-a-openmp-teorie)
    * [Příklady](procviceni.md#příklady)
    * [Procvičení časových složitostí](procviceni.md#procvičení-časových-složitostí-pro-různé-případy)
* [Doučko 19. - 22. 5.](doucovani.md)
* [WTF otázky](questions/wtf.md)