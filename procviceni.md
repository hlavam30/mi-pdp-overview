# Otázky k procvičení

## Obecné z úvodu a OpenMP teorie

Uveďte toky dat modelů paralelní architektury a jejich rozdíly.

Uveďte způsoby organizace paměti paralelní architektury a jejich rozdíly.

Uvěďte typy propojovacích sítí pamětí.

Popište výpočetní model PRAM.

Uveďte metody ošetření konfliktů při R/W do sdílené paměti u PRAM.

Popište výpočetní model APRAM.

Co je to ILP?

Uveďte datové závisloti, které brání ILP.

Uveďte programovací modely a popište je. (4)

Uveďte paralelní programovací šablony. (6)

Jaká jsou měřítka složitosti sekvenčních algoritmů?

Kdy je sekvenční algoritmus asymptoticky optimální?

Kdy je sekvenční algoritmus nejlepší známý?

Co je to paralelní běhový čas, na čem závisí?

Definujte paralelní zrychlení. Kdy je lineární?

Co je to superlineární zrychlení a kdy k němu dochází?

Definujte spodní mez na paralelní čas.

Definujte paralelní cenu. K čemu slouží, co představuje?

Definujte cenově optimální algoritmus.

Definujte paralelní efektivnost. Ukažte, že jde o zrychlení na jádro.

Kdy je paralelní algoritmus konstantně efektivní?

Ukažte, že cenově optimální $$\Leftrightarrow$$ lineární zrychlení $$\Leftrightarrow$$ konstantně efektivní.

Nakreslete typické závislosti $$T(n,p)$$, $$E(n,p)$$ a $$S(n,p)$$ na $$p$$.

Popište silnou a slabou škálovatelnost.

Definujte Amdahlův zákon.

Dokažte Amdahlův zákon.

Definujte Gustafsonův zákon.

Definujte izoefektivní funkce.

Z jakých součástí se skládá OpenMP? (3)

Na jakém mechanismu paralelizace v OpenMP funguje?

Pro jaké způsoby organizace paměti OpenMP funguje?

Vyjmenujte vlastnosti proměnných v OpenMP a uveďte jejich sémantiku. (6)

Definujte redukci, jak pracuje a jaké dva poddruhy existují.

Zapište časovou náročnost lineární a logaritmické redukce.

Čím lze ovlivnit počet vláken v paralelním regionu? (4)

Pomocí jakých klauzulí lze dosáhnout funkčního paralelismu?

Pomocí jaké klauzule lze dosáhnout iteračního paralelismu?

Popište `schedule()` klauzule `for`.

Vypište všechny schedule typy.

Popište výši režie u těchto schedule typů.

Popište `collapse()` klauzule for a implicitní chování.

Popište `ordered` a `nowait` klauzule `for`.

Co způsobí `#pragma omp task`?

Jak lze řídit task paralelizaci a co tato klauzule způsobí v případě negativní odpovědi?

Vyjmenujte synchronizační nástroje. (7)

Vyjmenujte všechny 4 typy atomických operací (direktiva `atomic`) v OpenMP a vysvětlete jejich sémantiku.

Charakterizujte C3S.

Co je to koncový stav, přípustný mezistav a přípustný koncový stav?

Co je to FSB-DFS a ASB-DFS?

Definujte DOP.

Co je cílem řešení DOP?

Z jakých stavů se u DOP provádí návrat? (4)

Co je to PP-DFS?

Popište statické rozdělení stavového prostoru.

Popište dynamické vyvažování zátěže.

Popište dynamický master-slave.

Kdy je paralelizace smysluplná?

Čeho chceme dosáhnout, aby byla smysluplná?

Jaké jsou zdroje neefektivity? (7)

Popište falešné sdílení.

Jak lze zabránit falešnému sdílení?

Jaká je hranice, při které k falešnému sdílení dochází?

Jak lze zabránit falešnému sdílení a zachovat paralelismus?

K čemu jsou pojmenované critical sekce?

Co je speciální atribut `MPI_STATUS_IGNORE` a k čemu slouží?

Co je speciální atribut `MPI_IN_PLACE` a k čemu slouží?

K čemu je určena funkce `MPI_Gatherv`? Popište její parametry `recvcounts` a `displs`.

Jaký je rozdíl mezi `#pragma omp single` a `#pragma omp master` a k čemu jsou určeny?

Vyjmenujte všechny možnosti kombinované paralelizace MPI a OpenMP.

## Příklady

Dokažte, že paralelní algoritmus s p procesory nad daty velikostí n má konstantní efektivnost právě když je cenově optimální.

---

Definujte matematicky pojem uzlově symetrický graf.

---

Díky jaké vlastnosti výpočtu bude následující fragment OpenMP kódu neefektivní na dnešních běžných vícejádrových procesorech? Navrhněte úpravy kódu tak, aby se tato neefektivnost co nejvíce odstranila a vysvětlete proč.  
```cpp
int A[n];  //predpokladame, ze n >> p
#pragma omp paralel for num_threads(p) shared(A) schedule(static, 1)
for (int i=0; i<n; i++) {
    A[i] = (i+4)*(3*i-1);
}
```

---

Uvažujte paralelní počítač s p procesory, na které je namapováno pole $$A[0, ..., p-1]$$ tak, že prvek $$A[i]$$ je na počátku umístěn v procesoru $$i$$. MPI pseudokódem popište korektní implementací paralelní komunikační operaci "cyklický posun pole $$A$$ o tři pozice".  
(= procesor $$i$$ začle prvek $$A[i]$$ procesoru $$i + 3\; (mod\; n)$$).  
Podrobně popište sémantiku použitých MPI operací a dokažte korektnost kódu.

---

Pro $$p < n$$ popište algoritmus paralelního segmentovaného prefixového součtu $$n$$-prvkového pole na všeportovém nepřímém úplném binárním stromu $$T$$ s $$p$$ procesory v listech. Dokažte jeho korektnost.  
Odvoďte jeho časovou složitost $$T(n,p)$$ v jednotkovém časovém modelu a pro zadanou efektivitu $$E_0$$ izoefektivní funkci $$\psi_2(n)$$.

---

Popište pseudokódem Foxův algoritmus paralelního násobení hustých čtvercových $$M(\sqrt{N},\sqrt{N})$$ matic $$C = AB$$ na všeportovém 2-D toroidu $$K(\sqrt{p},\sqrt{p})$$ procesů, kde $$1 \leq p \leq N$$, s plně duplexními kanály a WH přepínáním. Dokažte jeho korektnost. Odvoďte co nejpřesněji výraz pro paralelní čas $$T(n, p)$$, jestliže  
1. přenos paketu o velikost $$\mu$$ mezi 2 uzly ve vzdálenosti $$\delta$$ trvá $$t(\delta, \mu) = t_s + \delta t_d + \mu t_m$$
2. prvky matic mají velikost $$m$$
3. sekvenční lokální násobení dvou matic o velikosti $$r \times r$$ trvá čas $$\alpha r^3$$

---

Zformulujte Amdahlův zákon pro saturaci problému o velikosti $$n$$. Sekvenční doba vykonávání je $$T_A(n)$$.

---

Dokažte, že toroid a mřížka stejných dimenzí jsou kvaziizometrické.

---

Za jak dlouho proběhne následující OpenMP kód? Máme k dispozici šest jader procesoru.

```cpp
omp_set_threads(4);
#pragma omp parallel for private(i,tid) schedule(static)
for (int i=0; i<12; i++) {
  tid = get_num_thread();
  sleep(i+tid);
}
```

---

Mějme matici $$M(\sqrt{N},\sqrt{N})$$ namapovanou šachovnicově na $$p$$ procesorů. Popište MPI pseudokódem algoritmus, který spočte normu diagonály tak, aby tuto hodnotu měly všechny diagonální prvky. Pro jednoduchost předpokládejte, že $$sqrt{p}$$ je celé číslo.

---

Odvoďte krokovou spodní mez MC na 2D 1-portové WH mřížce. Pak popište krokově optimální algoritmus a dokažte, že optimální je. Algoritmus aplikujte na nakreslené mřížce.

---

Popište pseudokódem Cannonův algoritmus paralelního násobení hustých čtvercových $$M(\sqrt{N},\sqrt{N})$$ matic $$C = AB$$ na všeportovém 2-D toroidu $$K(\sqrt{p},\sqrt{p})$$ procesů, kde $$1 \leq p \leq N$$, s plně duplexními kanály a WH přepínáním. Dokažte jeho korektnost. Odvoďte co nejpřesněji výraz pro paralelní čas $$T(n, p)$$, jestliže  
1. přenos paketu o velikost $$\mu$$ mezi 2 uzly ve vzdálenosti $$\delta$$ trvá $$t(\delta, \mu) = t_s + \delta t_d + \mu t_m$$
2. prvky matic mají velikost $$m$$
3. sekvenční lokální násobení dvou matic o velikosti $$r \times r$$ trvá čas $$\alpha r^3$$

---

V uvedeném OpenMP kódu nalezněte všechny chyby. Navrhěnte opravu nalezených chyb a zdůvodněte je. Zhodnoďte efektivitu algoritmu a navrhněte vylepšení, pokud existuje.

```cpp
int A[m+1], B[n+1], C[n+m+1];
#pragma omp parallel for schedule(static)
for (int i=0; i<=m; i++) {
    for (int j=0; j<=n; j++) {
        C[i+j] += A[i]*B[j];
    }
}
```

---

Algebraicky pomocí Mortonovy křivky definujte vnoření hyperkrychle $$Q_2k$$ do mřížky $$M(2^k, 2^k)$$. Ukažte, jak se budou mapovat bitové řetězce. Vypočtěte spodní mez dilatace. Vypočtěte dilataci vnoření a okomentujte (ve vztahu k optimálnímu vnoření).

---

Určete časovou složitost prefixového paralelního součtu na EREW PRAM. Dokažte jeho korektnost.

---

Analyzujte uvedený OpenMP kód. Uveďte, proč nebude efektivní a popište, jak by se dal opravit. (Navrhněte alespoň dvě vylepšení)

```cpp
void par_quicksort_rec(int* A, long lo, long hi) {
    if (lo < hi) {
        long r = seq_partition(A, lo, hi);
        #pragma omp task 
        par_quicksort_rec(A, lo, r - 1);
        #pragma omp task 
        par_quicksort_rec(A, r + 1, hi);
    } 
}
void par_quicksort(int* A, long lo, long hi) { 
    #pragma omp parallel {
        #pragma omp single 
        par_quicksort_rec(A, lo, hi);
    }
}
```

---

Odvoďte spodní mez řazení $$n$$ čísel porovnávací metodou paralelně na $$p$$ procesorech, kde $$p < n$$.

---

Spočtěte jak dlouho poběží uvedený OpenMP kód. Čas na režii a ostatní instrukce zanedbáváme.

```cpp
set_threads(3);
#pragma omp parallel for private(i, pid) shedule(static, 2)
for (int i = 0; i < 15; i++) {
  int tid = get_num_thread();
  sleep(2*i + tid);
}
```

---

Definujte topologii obecného motýlka oBFn a zabaleného motýlka wBFn. Dokažte, že jsou vzájemně kvaziizometrické.

---

Spočtěte počet kroků a dolní meze pro AAB komunikaci na vše-portovém SF full-duplexním nekombinujícím 2-D toroidu.

---

Napište MPI pseudokód SPMD Master-Slave. Master dostane množimu $$m$$ tasků a distribuuje je $$p$$ otrokům, kde $$p << m$$. Každý task je definován množinou 10 int proměnných, trvá nepředvídatelně dlouho, jednotlivé tasky trvají různou dobu. Otrok masterovi vrací výsledek v podobě 1 int. Když masterovi dojdou tasky, explicitně musí otrokům přikázat konec práce. (nebo zhruba tak nějak, prosím o upřesnění)

---

Popište MPI pseudokódem algoritmus násobení $$(\sqrt{N}\times\sqrt{N})$$-matice $$A$$ šachovnicově mapované na mřížku $$M(\sqrt{p},\sqrt{p})$$, $$1 \leq p \leq N$$, $$(\sqrt{N}\times1)$$-vektorem $$x$$ mapovaným na diagonální procesy na mřížce. Výsledný vektor $$y = Ax$$ má být po skončení výpočtu mapován stejně, jako $$x$$.

---

*(PAR)* Algebraicky nebo graficky popište vnoření 2-D toroidu $$K(z_1, z_2)$$ do kružnice $$K(z_1, z_2)$$ s load = 1 a s co nejmenší dilatací. Rozhodněte, zda dilatace tohoto vnoření je optimální.

---

*(PAR)* Napište algoritmus násobení $$(\sqrt{N}\times\sqrt{N})$$-matice $$A$$ blokově šachovnicově mapované na mřížku $$M(\sqrt{p},\sqrt{p})$$, $$1 \leq p \leq N$$, $$(\sqrt{N}\times1)$$-vektorem $$x$$ mapovaným blokově na první sloupec mřížky. Výsledný vektor $$y = Ax$$ má být po skončení výpočtu mapován stejně, jako $$x$$. Odvoďte co nejpřesněji jeho paralelní čas $$T(N, p)$$ a pro zadanou efektivnost $$E_0$$ izoefektivní funkce $$\psi_1(p)$$ a $$\psi_2(N)$$. Předpokládejte, že 
1. $$M(\sqrt{p},\sqrt{p})$$ je 1-portová s WH přepínáním
2. sekvenční násobení ($$r \times r$$)-matice ($$r \times 1$$)-vektorem na 1 procesoru trvá čas $$\alpha r^2$$, kde $$\alpha$$ je konstanta.

---



## Procvičení časových složitostí pro různé případy

SF OAB na hyperkrychli

SF OAB vše-port 1-port více-D mřížka

WH OAB 1-port toroid

WH OAB 1-port více-D mřížka

WH OAB vše-port mřížka a toroid
- 1-D toroid
- 2-D toroid

WH MC 2-D mřížka

SF/WH kombinující OAS 1-port hyperkrychle

WH kombinující OAS mřížka a toroid

SF nekombinující AAB vše-port full-duplex 2-D a 3-D toroid

SF nekombinující AAB vše-port full-duplex 2-D mřížka

SF kombinující AAB 1-port hyperkrychle

WH AAB jakákoliv síť 

SF/WH AAS 
